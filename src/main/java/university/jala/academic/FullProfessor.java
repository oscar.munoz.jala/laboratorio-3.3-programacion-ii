package university.jala.academic;

public class FullProfessor extends Professors {
    public FullProfessor(String course) {
        super(course);
    }

    @Override
    public String learn() { //print the course taught by the full professor
        return "Dictating " + course.toLowerCase();
    }
}
