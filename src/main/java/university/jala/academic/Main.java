package university.jala.academic;

public class Main {
    public static void main(String[] args) {
        FullProfessor fullProfessor = new FullProfessor("SCIENCES");
        System.out.println("Full professor: "+ fullProfessor.learn());

        SubstituteProfessor substituteProfessor = new SubstituteProfessor("");
        substituteProfessor.assign("SCIENCES");
        System.out.println("\nSubstitute professor: "+ substituteProfessor.learn());

        substituteProfessor.assign("GYMNAST");
        System.out.println("\nSubstitute professor: "+ substituteProfessor.learn());

        substituteProfessor.assign("MUSIC");
        System.out.println("\nSubstitute professor: "+ substituteProfessor.learn());
    }
}
