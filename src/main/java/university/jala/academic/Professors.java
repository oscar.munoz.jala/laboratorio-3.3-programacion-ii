package university.jala.academic;

abstract class Professors implements IProfessors {
    protected String course;

    public Professors(String course) {
        this.course = course;
    } //constructor to create a new professor

    public abstract String learn(); //method to print the course taught by the professor
}
