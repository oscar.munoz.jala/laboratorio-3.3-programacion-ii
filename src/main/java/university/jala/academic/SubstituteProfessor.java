package university.jala.academic;

public class SubstituteProfessor extends Professors {

    public SubstituteProfessor(String courses) {
        super(courses);
    }

    public void assign(String course){
        this.course = course;
    }

    @Override
    public String learn() { //print the course taught by the substitute professor
        return "Dictating " + course.toLowerCase();
    }
}
