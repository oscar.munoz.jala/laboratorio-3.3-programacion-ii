package univesity.jala.academic;

import org.junit.Test;
import university.jala.academic.FullProfessor;
import university.jala.academic.SubstituteProfessor;

import static org.junit.Assert.assertEquals;
public class ProfessorTest {

    @Test
    public void createFullProfessor(){ //test to create a full professor with only one course
        FullProfessor fullProfessor = new FullProfessor("SCIENCES");
        fullProfessor.learn();
        assertEquals("Dictating sciences",fullProfessor.learn());
    }

    @Test
    public void createSubstituteProfessor(){ //test to create a substitute teacher with a course assignment
        SubstituteProfessor substituteProfessor = new SubstituteProfessor("SCIENCES");
        substituteProfessor.assign("MUSIC");
        assertEquals("Dictating music",substituteProfessor.learn());
    }
}
