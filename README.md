# Substitute teacher

The educational system has defined the categories of teachers for teaching, classified as follows:

- Titular: who is assigned to teach a subject for a defined contract period.
- Substitute or temporary: who is assigned in case the full professor is not available.

Characteristics:

- A full professor may teach only one course.
- A substitute professor can teach any course as needed.

## Output

![Output.png](Output.png)

## Workflow

```
---
title: Sample workflow
---
flowchart LR
start([start])

interface IProfessor:
   String learn() --> method to show the course that the professor is dictanding

abstract class Professor implements IProfessor:
   Atributes --> Course (Math, Music, Gymnastic, Sciences)
   Constructor(Course)
   learn()

class FullProfessor extendes Professor:
   Constructor(super(Course))
   assign(Course) --> method to assign a new course
   learn() --> return "Dictating " + course
   
class SubstituteProfessor extendes Professor:
   Constructor(super(Course))
   learn() --> return "Dictating " + course   

Main:
   new FullProfessor(Course)
   print fullProfessor.learn()
   
   new SubstituteProfessor()
   substituteProfessor.assign(Course)
   print substituteProfessor.learn()
   
([end])

```

## Diagram

![Diagram.png](Diagram.png)

## Execution

![Execution.png](Execution.png)

## Tests

![Test.png](Test.png)